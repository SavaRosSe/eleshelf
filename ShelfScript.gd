extends Control

signal button_pressed(name)

func _on_TextureButton_pressed():
	emit_signal("button_pressed", self.name)

func _on_Button_pressed():
	emit_signal("button_pressed", self.name)
