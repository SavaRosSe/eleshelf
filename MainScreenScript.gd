extends Control

#כול

# max scroll value
const SHELF_SIZE = 220
const SHELF_COUNT = 6
const MAX_SCROLL = SHELF_SIZE * SHELF_COUNT
const CONTAINER_VAR = MAX_SCROLL / SHELF_COUNT

var lang = 'en'
var edit_mode = false
var last = 0
var last2 = 0

var red = Color(1, 1, 1)
var green = Color(0, 1, 0)

func _ready():
	for i in range(1, 9): # 1 - 8
		get_node("Panel/Scroll/Closet/shelf_%s" % str(i)).connect("button_pressed", self, "shelf_clicked")

static func invert_string(s:String)->String:
	var chars_pool = PoolStringArray()
	var length = s.length()
	chars_pool.resize(length)
	for i in length:
		chars_pool[i] = s[i]
	chars_pool.invert()
	return chars_pool.join("")
	
func shelf_clicked(id):
	print(id)
	var name = get_node("Panel/Scroll/Closet/%s/Button" % id).text
	
	if not edit_mode:
		if name != " . . . ":
			if lang == 'en':
				handle_popup("%s is going to you!" % name)
			else:
				handle_popup("!אב %s" % name)
		else:
			if lang == 'en':
				handle_popup("The shelf is currently empty...")
			else:
				handle_popup("...קיר עגרכ ףדמה")
	else:
		$Change.popup()
		yield($Change, "popup_hide")
		if $Change/LineEdit.text.length() > 0:
			 get_node("Panel/Scroll/Closet/%s/Button" % id).text = $Change/LineEdit.text
		else:
			if lang == 'en':
				handle_popup("Shelf name cannot be empty")
			else:
				handle_popup("קיר ראשהל לוכי וניא ףדמ םש")

func _process(delta):
	var curr = round($Panel/Scroll.get_v_scrollbar().value / CONTAINER_VAR)
	
	$Panel2/HBoxContainer/Button3.modulate = green if edit_mode else red
	
	if last != curr:
		Input.vibrate_handheld(200)
	
	last = curr
	
	var total = $Panel/Scroll/Closet.get_child_count() - 1
	var shelfs = $Panel/Scroll/Closet.get_children()
	
	shelfs.remove(0)
	shelfs.remove(8)
	shelfs.remove(8)
	
	if (curr >= 7):
		curr = 7
	
	if curr >= 2:
		shelfs[curr - 2].get_node("TextureButton").texture_normal = preload("res://top.png")
		shelfs[curr - 2].get_node("Button").modulate.a = 0.5
	
	if curr >= 1:
		shelfs[curr - 1].get_node("TextureButton").texture_normal = preload("res://top2.png")
		shelfs[curr - 1].get_node("Button").modulate.a = 0.7
	
	if curr >= 3:
		shelfs[curr - 3].get_node("Button").modulate.a = 0
	
	shelfs[curr].get_node("TextureButton").texture_normal = preload("res://current.png")
	shelfs[curr].get_node("Button").modulate.a = 1
	
	if curr <= SHELF_COUNT:
		shelfs[curr + 1].get_node("TextureButton").texture_normal = preload("res://down1.png")
		shelfs[curr + 1].get_node("Button").modulate.a = 0.7
	
	if curr <= SHELF_COUNT - 1:
		shelfs[curr + 2].get_node("TextureButton").texture_normal = preload("res://down2.png")
		shelfs[curr + 2].get_node("Button").modulate.a = 0.5
	
	if curr <= SHELF_COUNT - 2:
		shelfs[curr + 3].get_node("Button").modulate.a = 0

func _on_about_pressed():
	$WindowDialog.popup()

func handle_popup(text):
	if $Popup.is_visible():
		$Popup/AnimationPlayer.stop()
		$Popup.hide()
		
	$Popup/Label.text = text
	$Popup.popup()

func _on_Dialog_about_to_show():
	$Popup/AnimationPlayer.play("fadeout")
	yield($Popup/AnimationPlayer, "animation_finished")
	$Popup.hide()


func _on_Button3_pressed(): # edit
	if edit_mode:
		edit_mode = false
	else: edit_mode = true
	
	if lang == 'en':
		handle_popup("Edit mode is %s" % str("ON" if edit_mode else "OFF"))
	else:
		handle_popup("%s-ל הנתשה יוניש בצמ" % str("יובכ" if not edit_mode else "קולד"))

func _on_changed():
	$Change.hide()
	$Change/LineEdit.text = ""

func _on_text_changed(new_text):
	var hebrew_lang = "קראטוןםפשדגכעיחלךףזסבהנמצתץ"
	var is_hebrew = false
	
	# if the last typed char is hebrew char.
	if new_text.length() > 0 and new_text[new_text.length() - 1] in hebrew_lang:
		is_hebrew = true
	
	if is_hebrew and new_text.length() > last2:
		$Change/LineEdit.caret_position = 0

	last2 = new_text.length()
	
func _on_pressed_backwards():
	$Change/LineEdit.text = ""

func _on_Insta_pressed():
	OS.shell_open("https://www.instagram.com/brevetti_netanya_unistream/")

func _on_Facebook_pressed():
	OS.shell_open("https://www.facebook.com/profile.php?id=100042605322760")


func _on_language_change():
	if lang == 'en':
		lang = 'he'
		handle_popup("תירבעל התנוש הפשה")
	else:
		lang = 'en'
		handle_popup("Language was changed to English")
	
	if lang == 'he':
		$Panel2/HBoxContainer/Button.text = 'הפש'
		$Panel2/HBoxContainer/Button2.text = 'וניילע הפיט'
		$Panel2/HBoxContainer/Button3.text = 'הנש'
		$WindowDialog/Label.text = "תויתרבחה תותשרב ונירחא ובקע\nונירחא בוקעל ולכותש\n!רתוי בוט ונתוא ריכהלו"
		$Change/Button.text = "םש הנש"
		$Change/Label.text = "ףדמ םש"
	else:
		$Panel2/HBoxContainer/Button.text = 'Language'
		$Panel2/HBoxContainer/Button2.text = 'About us'
		$Panel2/HBoxContainer/Button3.text = 'Edit'
		$WindowDialog/Label.text = "Follow our Facebook and Instagram\naccounts down bellow\nSo you could know us better\nand follow our progress!"
		$Change/Button.text = "Change name"
		$Change/Label.text = "Shelf name"
