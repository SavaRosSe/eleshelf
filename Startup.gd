extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Panel/AnimationPlayer.play("Start")
	yield($Panel/AnimationPlayer, "animation_finished")
	get_tree().change_scene("res://MainScreenScene.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
